;;;;; src/beta-side/beta-side.scm ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Copyright © 2013 Zhang Hongjiu
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;     http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;;; Description ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This file implements the alignment algorithm.
;;
;; Only two rows of scores and their corresponding alignments are kept in the
;; memory during the whole process. Scores and alignments are stored in immuta-
;; ble lists with their head cells storing scores. Choosing immutable lists
;; over mutable ones or vectors saves the time when adding new alignment units,
;; and should also save memory space if there are any common parts. But it
;; brings difficulties to final traceback.

;; TODO 2: Command line options.

;; TODO 6: Matrix output.

;; TODO 7: Test.

(import (scheme base)

        (only (srfi :1)
              cons*)
        (only (srfi :13)
              string-fold))

;;;; Constants ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define *nucleotide-sequence* #f)

(define *local-alignment* #f)

(define *gap-open-penalty* 11)

(define *gap-extension-penalty* 1)

;; Mapping of single characters to their indices.
(define *amino-acid-index-table*
  #(0 20 4 3 6 13 7 8 9 #f 11 10 12 2 #f 14 5 1 15 16 #f 19 17 22 18 21))

(define *BLOSUM62*
;;     A  R  N  D  C  Q  E  G  H  I  L  K  M  F  P  S  T  W  Y  V  B  Z  X
  #(#( 4 -1 -2 -2  0 -1 -1  0 -2 -1 -1 -1 -1 -2 -1  1  0 -3 -2  0 -2 -1  0)
    #(-1  5  0 -2 -3  1  0 -2  0 -3 -2  2 -1 -3 -2 -1 -1 -3 -2 -3 -1  0 -1)
    #(-2  0  6  1 -3  0  0  0  1 -3 -3  0 -2 -3 -2  1  0 -4 -2 -3  3  0 -1)
    #(-2 -2  1  6 -3  0  2 -1 -1 -3 -4 -1 -3 -3 -1  0 -1 -4 -3 -3  4  1 -1)
    #( 0 -3 -3 -3  9 -3 -4 -3 -3 -1 -1 -3 -1 -2 -3 -1 -1 -2 -2 -1 -3 -3 -2)
    #(-1  1  0  0 -3  5  2 -2  0 -3 -2  1  0 -3 -1  0 -1 -2 -1 -2  0  3 -1)
    #(-1  0  0  2 -4  2  5 -2  0 -3 -3  1 -2 -3 -1  0 -1 -3 -2 -2  1  4 -1)
    #( 0 -2  0 -1 -3 -2 -2  6 -2 -4 -4 -2 -3 -3 -2  0 -2 -2 -3 -3 -1 -2 -1)
    #(-2  0  1 -1 -3  0  0 -2  8 -3 -3 -1 -2 -1 -2 -1 -2 -2  2 -3  0  0 -1)
    #(-1 -3 -3 -3 -1 -3 -3 -4 -3  4  2 -3  1  0 -3 -2 -1 -3 -1  3 -3 -3 -1)
    #(-1 -2 -3 -4 -1 -2 -3 -4 -3  2  4 -2  2  0 -3 -2 -1 -2 -1  1 -4 -3 -1)
    #(-1  2  0 -1 -3  1  1 -2 -1 -3 -2  5 -1 -3 -1  0 -1 -3 -2 -2  0  1 -1)
    #(-1 -1 -2 -3 -1  0 -2 -3 -2  1  2 -1  5  0 -2 -1 -1 -1 -1  1 -3 -1 -1)
    #(-2 -3 -3 -3 -2 -3 -3 -3 -1  0  0 -3  0  6 -4 -2 -2  1  3 -1 -3 -3 -1)
    #(-1 -2 -2 -1 -3 -1 -1 -2 -2 -3 -3 -1 -2 -4  7 -1 -1 -4 -3 -2 -2 -1 -2)
    #( 1 -1  1  0 -1  0  0  0 -1 -2 -2  0 -1 -2 -1  4  1 -3 -2 -2  0  0  0)
    #( 0 -1  0 -1 -1 -1 -1 -2 -2 -1 -1 -1 -1 -2 -1  1  5 -2 -2  0 -1 -1  0)
    #(-3 -3 -4 -4 -2 -2 -3 -2 -2 -3 -2 -3 -1  1 -4 -3 -2 11  2 -3 -4 -3 -2)
    #(-2 -2 -2 -3 -2 -1 -2 -3  2 -1 -1 -2 -1  3 -3 -2 -2  2  7 -1 -3 -2 -1)
    #( 0 -3 -3 -3 -1 -2 -2 -3 -3  3  1 -2  1 -1 -2 -2  0 -3 -1  4 -3 -2 -1)
    #(-2 -1  3  4 -3  0  1 -1  0 -3 -4  0 -3 -3 -2  0 -1 -4 -3 -3  4  1 -1)
    #(-1  0  0  1 -3  3  4 -2  0 -3 -3  1 -1 -3 -1  0 -1 -3 -2 -2  1  4 -1)
    #( 0 -1 -1 -1 -2 -1 -1 -1 -1 -1 -1 -1 -1 -1 -2  0  0 -2 -1 -1 -1 -1 -1)))
;;     A  R  N  D  C  Q  E  G  H  I  L  K  M  F  P  S  T  W  Y  V  B  Z  X

;;;; Common procedures ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define prompt-read
  (lambda (str)
    (display str)
    (flush-output-port (current-output-port))
    (read-line)))

(define alphabet-index
  (lambda (char)
    (- (char->integer (char-upcase char))
       65)))

(define amino-acid-index
  (lambda (char)
    (vector-ref *amino-acid-index-table* (alphabet-index char))))

(define match-amino-acid
  (lambda (unit0 unit1)
    (vector-ref (vector-ref *BLOSUM62* (amino-acid-index unit0))
                (amino-acid-index unit1))))

(define match-nucleotide
  (lambda (unit0 unit1)
    (if (equal? unit0 unit1)
      5
      -4)))

(define make-decision
  (lambda (best-dec . decs)
    (cond
      ((null? decs)
       best-dec)
      ((< (car best-dec)
          (caar decs))
       (apply make-decision decs))
      (else
       (apply make-decision best-dec (cdr decs))))))

(define matrix-iter
  (lambda (make-row0 make-rows rows-fill! report)
    (lambda (match seq0 seq1)
      (let ((max-row (string-length seq1))
            (max-col (string-length seq0)))
        (let loop ((row  1)
                   (col  1)
                   (rows (make-row0 max-col)))
          (cond
            ((and (> col max-col)
                  (= row max-row))
             (call-with-values rows report))
            ((> col max-col)
             (loop (+ 1 row)
                   1
                   (call-with-values rows make-rows)))
            (else
             (let* ((unit0 (string-ref seq0 (- col 1)))
                    (unit1 (string-ref seq1 (- row 1)))
                    (score (match unit0 unit1)))
               (loop row
                     (+ 1 col)
                     (call-with-values rows
                                       (rows-fill! row col score)))))))))))

(define display-by-line
  (lambda (seq0 seq1)
    (let ((len (length seq0)))
      (let loop ((start 0))
        (if (< start (- len 79))
          (begin
            (display (string-copy seq0 start (+ start 79)))
            (newline)
            (display (string-copy seq1 start (+ start 79)))
            (newline)
            (newline)
            (loop (+ end 79)))
          (begin
            (display (string-copy seq0 start len))
            (newline)
            (display (string-copy seq1 start len))
            (newline)))))))

(define output-alignment
  (lambda (seq0 seq1 end-pos0 end-pos1 result)
    (let* ((score       (car result))
           (align       (cdr result))
           (len         (length align))
           (output-seq0 (make-string len))
           (output-seq1 (make-string len)))
      (let loop ((i        (- len 1))
                 (pos0     (- end-pos0 1))
                 (pos1     (- end-pos1 1))
                 (align-ls align))
        (unless (null? align-ls)
          (case (car align-ls)
            ((ext)
             (string-set! output-seq0 i (char-upcase (string-ref seq0 pos0)))
             (string-set! output-seq1 i (char-upcase (string-ref seq1 pos1)))
             (loop (- i 1)
                   (- pos0 1)
                   (- pos1 1)
                   (cdr align-ls)))
            ((gap0)
             (string-set! output-seq0 i #\-)
             (string-set! output-seq1 i (char-upcase (string-ref seq1 pos1)))
             (loop (- i 1)
                   pos0
                   (- pos1 1)
                   (cdr align-ls)))
            ((gap1)
             (string-set! output-seq0 i (char-upcase (string-ref seq0 pos0)))
             (string-set! output-seq1 i #\-)
             (loop (- i 1)
                   (- pos0 1)
                   pos1
                   (cdr align-ls))))))
      (newline)
      (display "Score: ")
      (display score)
      (newline)
      (newline)
      (display-by-line output-seq0 output-seq1))))

;;;; Global alignment with constant gap penalty ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; The first line and column is filled by negative scores, which means adding
;; gaps at the start of either sequence.

(define make-global-constant-row0
  (lambda (max-col)
    (lambda ()
      (let ((row0 (make-vector (+ 1 max-col)))
            (row1 (make-vector (+ 1 max-col))))
        (let loop ((col 0))
          (when (<= col max-col)
            (let ((score (- (* *gap-extension-penalty* col)))
                  (align (make-list col 'gap1)))
              (vector-set! row0 col (cons score align))
              (loop (+ 1 col)))))
        (vector-set! row1 0 `(,(- *gap-extension-penalty*) gap0))
        (values row0 row1)))))

(define make-global-constant-rows
  (lambda (prev-row-2 prev-row-1)
    (lambda ()
      (let* ((len       (vector-length prev-row-1))
             (curr-row  (make-vector len))
             (prev-col0 (vector-ref prev-row-1 0))
             (score     (- (car prev-col0)
                           *gap-extension-penalty*))
             (align     (cons 'gap0 (cdr prev-col0))))
        (vector-set! curr-row 0 (cons score align))
        (values prev-row-1 curr-row)))))

(define global-constant-rows-fill!
  (lambda (row col score)
    (lambda (prev-row curr-row)
      (lambda ()
        (let* (;; Past decisions
               (top-left   (vector-ref prev-row (- col 1)))
               (top        (vector-ref prev-row col))
               (left       (vector-ref curr-row (- col 1)))
               ;; Scores and alignments of present decisions
               (ext-score  (+ score (car top-left)))
               (ext-align  (cons 'ext (cdr top-left)))
               (gap0-score (- (car top)
                              *gap-extension-penalty*))
               (gap0-align (cons 'gap0 (cdr top)))
               (gap1-score (- (car left)
                              *gap-extension-penalty*))
               (gap1-align (cons 'gap1 (cdr left)))
               (decis      (make-decision (cons ext-score ext-align)
                                          (cons gap0-score gap0-align)
                                          (cons gap1-score gap1-align))))
          (vector-set! curr-row col decis)
          (values prev-row curr-row))))))

(define global-constant-report
  (lambda (prev-row curr-row)
    (let* ((len  (vector-length curr-row))
           (end0 (- len 1))
           (end1 (/ (car (vector-ref curr-row 0))
                    (- *gap-extension-penalty*))))
      (list end0 end1 (vector-ref curr-row end0)))))

(define global-constant-align
  (matrix-iter make-global-constant-row0
               make-global-constant-rows
               global-constant-rows-fill!
               global-constant-report))

;;;; Global alignment with linear gap penalty ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This mode is similar with global alignment, constant gap penalty mode, ex-
;; cept Osamu Gotoh's algorithm is implemented here.

(define make-global-linear-row0
  (lambda (max-col)
    (lambda ()
      (let* (;; New rows
             (score-row0 (make-vector (+ 1 max-col)))
             (gap0-row0  (make-vector (+ 1 max-col)))
             (row0       (vector score-row0 gap0-row0))
             (score-row1 (make-vector (+ 1 max-col)))
             (gap0-row1  (make-vector (+ 1 max-col)))
             (gap1-row1  (make-vector (+ 1 max-col)))
             (row1       (vector score-row1 gap0-row1 gap1-row1))
             ;; Cell 0 of rows in ROW1
             (cell-score (- *gap-open-penalty*))
             (cell-align '(gap0))
             (gap0-score (- cell-score *gap-extension-penalty*))
             (gap0-align (cons 'gap0 cell-align))
             (gap1-score (- cell-score *gap-open-penalty*))
             (gap1-align (cons 'gap1 cell-align)))
        (vector-set! score-row0 0 '(0))
        (vector-set! gap0-row0 0 `(,(- *gap-open-penalty*) gap0))
        (let loop ((col 1))
          (when (<= col max-col)
            (let* ((cell-score (- (* *gap-extension-penalty* (- 1 col))
                                  *gap-open-penalty*))
                   (cell-align (make-list col 'gap1))
                   (gap0-score (- cell-score *gap-open-penalty*))
                   (gap0-align (cons 'gap0 cell-align)))
              (vector-set! score-row0 col (cons cell-score cell-align))
              (vector-set! gap0-row0 col (cons gap0-score gap0-align))
              (loop (+ 1 col)))))
        (vector-set! score-row1 0 (cons cell-score cell-align))
        (vector-set! gap0-row1 0 (cons gap0-score gap0-align))
        (vector-set! gap1-row1 0 (cons gap1-score gap1-align))
        (values row0 row1)))))

(define make-global-linear-rows
  (lambda (prev-row-2 prev-row)
    (lambda ()
      (let* (;; Matrix seperation
             (prev-score-row (vector-ref prev-row 0))
             (prev-gap0-row  (vector-ref prev-row 1))
             ;; New rows
             (len            (vector-length prev-score-row))
             (curr-score-row (make-vector len))
             (curr-gap0-row  (make-vector len))
             (curr-gap1-row  (make-vector len))
             (curr-row       (vector curr-score-row
                                     curr-gap0-row
                                     curr-gap1-row))
             ;; Cell 0 of rows in CURR-ROW
             (prev-col0      (vector-ref prev-score-row 0))
             (cell-score     (- (car prev-col0)
                                *gap-extension-penalty*))
             (cell-align     (cons 'gap0 (cdr prev-col0)))
             (gap0-score     (- cell-score *gap-extension-penalty*))
             (gap0-align     (cons 'gap0 cell-align))
             (gap1-score     (- cell-score *gap-open-penalty*))
             (gap1-align     (cons 'gap1 cell-align)))
        (vector-set! curr-score-row 0 (cons cell-score cell-align))
        (vector-set! curr-gap0-row 0 (cons gap0-score gap0-align))
        (vector-set! curr-gap1-row 0 (cons gap1-score gap1-align))
        (values prev-row curr-row)))))

(define global-linear-rows-fill!
  (lambda (row col score)
    (lambda (prev-row curr-row)
      (lambda ()
        (let* (;; Group seperation
               (prev-score-row  (vector-ref prev-row 0))
               (prev-gap0-row   (vector-ref prev-row 1))
               ;; New rows
               (curr-score-row  (vector-ref curr-row 0))
               (curr-gap0-row   (vector-ref curr-row 1))
               (curr-gap1-row   (vector-ref curr-row 2))
               ;; Past decisions
               (top-left        (vector-ref prev-score-row (- col 1)))
               (gap0            (vector-ref prev-gap0-row col))
               (gap1            (vector-ref curr-gap1-row (- col 1)))
               ;; Scores and alignments of present decisions
               (ext-score       (+ score (car top-left)))
               (ext-align       (cons 'ext (cdr top-left)))
               (ext             (cons ext-score ext-align))
               (decis           (make-decision ext gap0 gap1))
               (gap0-ext-score  (- (car gap0)
                                   *gap-extension-penalty*))
               (gap0-ext-align  (cons 'gap0 (cdr gap0)))
               (gap0-ext        (cons gap0-ext-score gap0-ext-align))
               (gap0-open-score (- (car ext)
                                   *gap-open-penalty*))
               (gap0-open-align (cons 'gap0 (cdr ext)))
               (gap0-open       (cons gap0-open-score gap0-open-align))
               (gap0-decis      (make-decision gap0-ext gap0-open))
               (gap1-ext-score  (- (car gap1)
                                   *gap-extension-penalty*))
               (gap1-ext-align  (cons 'gap1 (cdr gap1)))
               (gap1-ext        (cons gap1-ext-score gap1-ext-align))
               (gap1-open-score (- (car ext)
                                   *gap-open-penalty*))
               (gap1-open-align (cons 'gap1 (cdr ext)))
               (gap1-open       (cons gap1-open-score gap1-open-align))
               (gap1-decis      (make-decision gap1-ext gap1-open)))
          (vector-set! curr-score-row col decis)
          (vector-set! curr-gap0-row col gap0-decis)
          (vector-set! curr-gap1-row col gap1-decis)
          (values prev-row curr-row))))))

(define global-linear-report
  (lambda (prev-row curr-row)
    (let* ((score-row (vector-ref curr-row 0))
           (len       (vector-length score-row))
           (end0      (- len 1))
           (end1      (+ 1 (/ (+ (car (vector-ref score-row 0))
                                 *gap-open-penalty*)
                              (- *gap-extension-penalty*)))))
      (list end0 end1 (vector-ref score-row end0)))))

(define global-linear-align
  (matrix-iter make-global-linear-row0
               make-global-linear-rows
               global-linear-rows-fill!
               global-linear-report))

;;;; Local alignment with constant gap penalty ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Although gaps are added gaps at the start of either sequence, because no ne-
;; gative score is put in the matrix, the first line and column is filled with
;; zero.

(define make-local-constant-row0
  (lambda (max-col)
    (lambda ()
      (let ((len (+ 1 max-col))
            (prev-row (make-vector len '(0)))
            (curr-row (make-vector len '(0))))
      (values prev-row curr-row 0 0 '(0))))))

(define make-local-constant-rows
  (lambda (prev-row-2 prev-row-1 best-end0 best-end1 best-align)
    (lambda ()
      (let* ((len      (vector-length prev-row-1))
             (curr-row (make-vector len)))
        (vector-set! curr-row 0 '(0))
        (values prev-row-1 curr-row best-end0 best-end1 best-align)))))

(define local-constant-rows-fill!
  (lambda (row col score)
    (lambda (prev-row curr-row best-end0 best-end1 best-align)
      (lambda ()
        (let* (;; Past decisions
               (top-left   (vector-ref prev-row (- col 1)))
               (top        (vector-ref prev-row col))
               (left       (vector-ref curr-row (- col 1)))
               ;; Scores and alignments of present decisions
               (ext-score  (+ score (car top-left)))
               (ext-align  (cons 'ext (cdr top-left)))
               (ext        (cons ext-score ext-align))
               (gap0-score (- (car top)
                              *gap-extension-penalty*))
               (gap0-align (cons 'gap0 (cdr top)))
               (gap0       (cons gap0-score gap0-align))
               (gap1-score (- (car left) *gap-extension-penalty*))
               (gap1-align (cons 'gap1 (cdr left)))
               (gap1       (cons gap1-score gap1-align))
               (decis      (make-decision '(0) ext gap0 gap1)))
          (vector-set! curr-row col decis)
          (if (> (car decis) (car best-align))
            (values prev-row curr-row col row decis)
            (values prev-row curr-row best-end0 best-end1 best-align)))))))

(define local-constant-report
  (lambda args
    (cddr args)))

(define local-constant-align
  (matrix-iter make-local-constant-row0
               make-local-constant-rows
               local-constant-rows-fill!
               local-constant-report))

;;;; Local alignment with linear gap penalty ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This mode is similar with local alignment, constant gap penalty mode, except
;; Osamu Gotoh's algorithm is implemented here.

(define make-local-linear-row0
  (lambda (max-col)
    (lambda ()
      (let* ((len        (+ 1 max-col))
             (score-row0 (make-vector len '(0)))
             (gap0-row0  (make-vector len '(0)))
             (row0       (vector score-row0 gap0-row0))
             (score-row1 (make-vector len '(0)))
             (gap0-row1  (make-vector len '(0)))
             (gap1-row1  (make-vector len '(0)))
             (row1       (vector score-row1 gap0-row1 gap1-row1)))
        (values row0 row1 0 0 '(0))))))

(define make-local-linear-rows
  (lambda (prev-row-2 prev-row best-end0 best-end1 best-align)
    (lambda ()
      (let* ((prev-score-row (vector-ref prev-row 0))
             (len            (vector-length prev-score-row))
             (curr-score-row (make-vector len '(0)))
             (curr-gap0-row  (make-vector len '(0)))
             (curr-gap1-row  (make-vector len '(0)))
             (curr-row       (vector curr-score-row
                                     curr-gap0-row
                                     curr-gap1-row)))
        (values prev-row curr-row best-end0 best-end1 best-align)))))

(define local-linear-rows-fill!
  (lambda (row col score)
    (lambda (prev-row curr-row best-end0 best-end1 best-align)
      (lambda ()
        (let* (;; Row seperation
               (prev-score-row  (vector-ref prev-row 0))
               (prev-gap0-row   (vector-ref prev-row 1))
               ;; New rows
               (curr-score-row  (vector-ref curr-row 0))
               (curr-gap0-row   (vector-ref curr-row 1))
               (curr-gap1-row   (vector-ref curr-row 2))
               ;; Past decisions
               (top-left        (vector-ref prev-score-row (- col 1)))
               (gap0            (vector-ref prev-gap0-row col))
               (gap1            (vector-ref curr-gap1-row (- col 1)))
               ;; Scores and alignments of present decisions
               (ext-score       (+ score (car top-left)))
               (ext-align       (cons 'ext (cdr top-left)))
               (ext             (cons ext-score ext-align))
               (decis           (make-decision ext gap0 gap1 '(0)))
               (gap0-ext-score  (- (car gap0)
                                   *gap-extension-penalty*))
               (gap0-ext-align  (cons 'gap0 (cdr gap0)))
               (gap0-ext        (cons gap0-ext-score gap0-ext-align))
               (gap0-open-score (- (car ext)
                                   *gap-open-penalty*))
               (gap0-open-align (cons 'gap0 (cdr gap0)))
               (gap0-open       (cons gap0-open-score gap0-open-align))
               (gap0-decis      (make-decision gap0-ext gap0-open '(0)))
               (gap1-ext-score  (- (car gap1)
                                   *gap-extension-penalty*))
               (gap1-ext-align  (cons 'gap1 (cdr gap1)))
               (gap1-ext        (cons gap1-ext-score gap1-ext-align))
               (gap1-open-score (- (car ext)
                                   *gap-open-penalty*))
               (gap1-open-align (cons 'gap1 (cdr gap1)))
               (gap1-open       (cons gap1-open-score gap1-open-align))
               (gap1-decis      (make-decision gap1-ext gap1-open '(0))))
          (vector-set! (vector-ref curr-row 0) col decis)
          (vector-set! curr-gap0-row col gap0-decis)
          (vector-set! curr-gap1-row col gap1-decis)
          (if (> (car decis)
                 (car best-align))
            (values prev-row curr-row col row decis)
            (values prev-row curr-row best-end0 best-end1 best-align)))))))

(define local-linear-report
  (lambda args
    (cddr args)))

(define local-linear-align
  (matrix-iter make-local-linear-row0
               make-local-linear-rows
               local-linear-rows-fill!
               local-linear-report))

;;;; Main procedure ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(let* ((seq0   (prompt-read "Please input Sequence 1: "))
       (seq1   (prompt-read "Please input Sequence 2: "))
       (match  (if *nucleotide-sequence*
                 match-nucleotide
                 match-amino-acid))
       (align  (cond
                 ((and *local-alignment*
                       (= *gap-open-penalty* *gap-extension-penalty*))
                  local-constant-align)
                 (*local-alignment*
                  local-linear-align)
                 ((= *gap-open-penalty* *gap-extension-penalty*)
                  global-constant-align)
                 (else
                  global-linear-align)))
       (result (align match seq0 seq1)))
  (apply output-alignment seq0 seq1 result))