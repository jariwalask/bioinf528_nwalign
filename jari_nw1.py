#!/usr/bin/python

# Global alignment using Needleman-Wunsch, and Gotoh's affine-gap penalty method
#
##### Usage ####################################################################
#
# $python jari_align.py seq1 seq2 BLOSUM62.txt
#
# Input files can be protein or DNA sequences, with or without FASTA header;
# given files 'seq1' and 'seq2' are protein sequences with FASTA headers.
# 
# User-defined scoring matrix can be given as well, by replacing 'BLOSUM62.txt'
# above. If no scoring matrix is mentioned, BLOSUM62 is used by default.
#
# Output is in srspair alignment format (can be checked at: 
# http://emboss.sourceforge.net/docs/themes/alnformats/align.srspair)
# 
# Score matrix is initialized as per Gotoh (1982):
# 0, -11, -12, -13 ... (instead of) 0, -1, -2, -3 ...
# 
# For gap open on both sequences: W(k) = Go+Ge*k1 + Go+Ge*k2
#
##### References ####################################################################
# 
# Needleman SB, Wunsch CD. A general method applicable to the search for simila-
# rities in the amino acid sequence of two proteins. J Mol Biol. 1970; 48(3):
# 443-53.
# 
# Gotoh O. An improved algorithm for matching biological sequences. J Mol Biol.
# 1982; 162(3): 705-8.

import sys, os, datetime

# Gap open, and extension penalties
_gopen_ = 1
_gextend_ = 1
lines1 = []
lines2 = []
_lenseq1_ = _lenseq2_ = 0
_letters_ = []
sub_matrix = {}
global __file__
global Smat, Hmat, Vmat

def read_input():
	"""Read the two sequences to be aligned"""
	input_length = len(sys.argv)
	if input_length < 3:
		if input_length < 2:
                	print 'No sequence files given'
                	sys.exit()
		else:
			print 'Second sequence file not given'
			sys.exit()
	f1 = open(sys.argv[1])
	global lines1
	global lines2
	lines1 = f1.readlines()
	f1.close()
	if(lines1[0][0] == '>'):
		lines1 = lines1[1].replace('\n','').replace(' ','')
	else:
		lines1 = lines1[0].replace('\n','').replace(' ','')

	f2 = open(sys.argv[2])
	lines2 = f2.readlines()
	f2.close()
	if(lines2[0][0] == '>'):
		lines2 = lines2[1].replace('\n','')
	else:
		lines2 = lines2[0].replace('\n','')

	global _lenseq1_
	_lenseq1_ = len(lines1)
	global _lenseq2_
	_lenseq2_ = len(lines2)

	read_matrix() #Read the scoring matrix

def read_matrix():
	"""Read the scoring matrix to be used,
	   else try to use BLOSUM62.txt by default,
	   else file not found """
	global __file__
	try:
		__file__ = sys.argv[3]
		f3 = open(__file__)
	except IndexError:
		#Open the blosum txt by default
		try:
			__file__ = 'BLOSUM62.txt'
			f3 = open(os.path.abspath(__file__))
		except IOError:
			print 'BLOSUM62.txt file not found!'
			print 'Please download BLOSUM matrix from http://zhanglab.ccmb.med.umich.edu/NW-align/BLOSUM62.txt'
			sys.exit()
		else:
			lines3 = f3.readlines()
			f3.close()
	except IOError:
		print('File: \'%s\' not found!' % sys.argv[3] )
		print 'Please download BLOSUM matrix from http://zhanglab.ccmb.med.umich.edu/NW-align/BLOSUM62.txt'
		sys.exit()
	else:
		lines3 = f3.readlines()
		f3.close()
	
	if(lines3[0][0] == '#'):
		del(lines3[0])
	
	global _letters_
	_letters_ = lines3[0].replace('\r\n','').split() #Split 1st element of lines3 list to get colheaders
	
	for i in range(1,len(lines3)):
		row = lines3[i].replace('\r\n','').split()
		rowHeader = row[0]
		del(row[0]) #row = del(row[0])
		for j in range(0,len(row)):
			#Make row+col key for our dict
			key = rowHeader + "2" + _letters_[j] #A2B means A is substituted by B
			global sub_matrix
			sub_matrix[key] = row[j]

	return sub_matrix

def init_matrix(r, c, filler):
	###########################################
	# Initialize and return a matrix, all cells set to 'filler'
	# r: number of rows
	# c: number of columns
	# Matrix of the form: mat[j][i]; j=row-wise; i=column-wise
	###########################################
	
	matrix = [[filler for i in range(c)] for j in range(r)]
	return matrix

def print_matrix(matrix):
	###########################################
	# Print matrix [by row]
	###########################################
	for i in range(0, len(matrix)):
		for j in range(0, len(matrix[0])):
			print("%s " % matrix[i][j]),
		print("\n")

def align_scores(lenseq1, lenseq2):
	###########################################
	# Needleman-Wunsch algorithm, with jumps 
	# according to Gotoh (1982)
	###########################################
	global Smat, Hmat, Vmat, Tmat
	Smat = init_matrix(lenseq2+1,lenseq1+1,0) #cols = len(seq1)
	Hmat = init_matrix(lenseq2+1,lenseq1+1,0) #Deletion/gap in seq2
	Vmat = init_matrix(lenseq2+1,lenseq1+1,0) #Insertion/gap in seq1
	Tmat = init_matrix(lenseq2+1,lenseq1+1,0) #traceback matrix, -1=del in seq2; 1=ins in seq2;0=match
	#print("seq1: %s seq2: %s" % (lenseq1,lenseq2))
	start_score = -(_gopen_) #start_score = 0
	#i:column-wise increment
	for i in range(1, len(Smat[0])): #where should the range start for Smat
		Smat[0][i] = start_score
		start_score -= _gextend_
		Tmat[0][i] = -1
	start_score = -(_gopen_) #start_score = 0
	#j:row-wise increment
	for j in range(1, len(Smat)):
		Smat[j][0] = start_score
		start_score -= _gextend_
		Tmat[j][0] = 1

	for i in range(1,lenseq1+1):
		for j in range(1,lenseq2+1): #first column, adjacent to our initialized scores
			
			Smat[j][i] = compare_state_score(j,i) #get match/mismatch score in compare mode

			diag_sum = int(Smat[j][i]) + int(Smat[j-1][i-1]) #diagonal add
			
			Smat[j][i] = max(horizontal(i,j), vertical(i,j), diag_sum)

			if Smat[j][i] == diag_sum: Tmat[j][i] = 0 #do nothing
			elif Smat[j][i] == Hmat[j][i]: Tmat[j][i] = -1 #del in seq2
			else: Tmat[j][i] = 1
	traceback()

def horizontal(col, row):
	###########################################
	# Fill the horizontal jump matrix,
	# according to Gotoh (1982)
	###########################################
	if col==1: #base case
		Hmat[row][col] = int(Smat[row][col-1]) - _gopen_		
		return Hmat[row][col]
	else:
		Hmat[row][col] = max(int(Smat[row][col-1]) - gap_score_eq(0), Hmat[row][col-1] - _gextend_)
		return Hmat[row][col]

def vertical(col, row):
	###########################################
	# Fill the vertical jump matrix,
	# according to Gotoh (1982)
	###########################################
	if row==1: #base case
		Vmat[row][col] = int(Smat[row-1][col]) - _gopen_
		return Vmat[row][col]
	else:
		Vmat[row][col] = max(int(Smat[row-1][col]) - gap_score_eq(0), Vmat[row-1][col] - _gextend_)
		return Vmat[row][col]


def compare_state_score(row, col):
	###########################################
	# Fetches score of the two residues
	# under comparison from the scoring matrix
	###########################################
	r_elem = lines2[row-1]
	c_elem = lines1[col-1]
	score_value = sub_matrix[r_elem+'2'+c_elem]
	return score_value

def gap_score_eq(ext_width):
	return _gopen_ + (ext_width * _gextend_)


def traceback():
	###########################################
	# Trace back the traceback matrix to 
	# generate and print the alignment
	###########################################
	i = _lenseq1_
	j = _lenseq2_

	global aligned_seq1, aligned_seq2, aligned_symb
	aligned_seq1 = []
	aligned_seq2 = []
	aligned_symb = []
	
	while(i != 0 or j !=0):

		if Tmat[j][i] == 0:
			aligned_seq1.insert(0,lines1[i-1])
			aligned_seq2.insert(0,lines2[j-1])
			#print("\nseq1[i]: %s;seq2[j]: %s" % (lines1[i-1],lines2[j-1]))
			if lines1[i-1] == lines2[j-1]:
				aligned_symb.insert(0, ':')
			else:
				aligned_symb.insert(0, ' ')
			i -= 1
			j -= 1
		#del in seq2
		elif Tmat[j][i] == -1:
			while(Hmat[j][i] == Hmat[j][i-1] - 1):
				aligned_seq1.insert(0,lines1[i-1])
				aligned_seq2.insert(0,'-')
				aligned_symb.insert(0, ' ')
				i -= 1
			aligned_seq1.insert(0,lines1[i-1])
			aligned_seq2.insert(0,'-')
			aligned_symb.insert(0, ' ')
			i -= 1
		#insert in seq2/del in seq1
		else:
			while(Vmat[j][i] == Vmat[j-1][i] - 1):
				aligned_seq1.insert(0,'-')
				aligned_seq2.insert(0,lines2[j-1])
				aligned_symb.insert(0, ' ')
				j -= 1
			aligned_seq1.insert(0,'-')
			aligned_seq2.insert(0,lines2[j-1])
			aligned_symb.insert(0, ' ')
			j -= 1

	print_meta_data()
	
	print("\n%s\n%s\n%s\n" % (''.join(aligned_seq1), ''.join(aligned_symb), ''.join(aligned_seq2)))
			

def print_meta_data():
	print("\n################################################")
	print("# Rundate: %s" % datetime.datetime.now().strftime("%A, %d. %B %Y %I:%M%p"))
	print("# Align_format: srspair")
	print("# Gap open: %s" % _gopen_)
	print("# Gap extend: %s" % _gextend_)
	print("################################################")
	print("#===============================================")

	aligned_length = len(aligned_seq2)
	seq_identity = ''.join(aligned_symb).count(':')
	gaps = ''.join(aligned_seq1).count('-') + ''.join(aligned_seq2).count('-')
	print("\n# Length of sequence 1: %s\n# Length of sequence 2: %s" % (_lenseq1_, _lenseq2_))
	print("# Matrix: %s\n" % __file__)
	print("# Length (alignment+gaps): %s" % aligned_length)
	print("# Identity: (%s/%s) %s%%" % (seq_identity, aligned_length, round(((seq_identity / float(aligned_length))*100), 2)))
	print("# Gaps: (%s/%s) %s%%" % (gaps, aligned_length, round(((gaps / float(aligned_length))*100), 2)))
	print("# Score: %s" % Smat[_lenseq2_][_lenseq1_])
	print("#===============================================")


read_input()

align_scores(_lenseq1_,_lenseq2_)
print("#-----------------------------------------------")
print("#-----------------------------------------------")

#print_matrix(Smat)
#print_matrix(Tmat)




